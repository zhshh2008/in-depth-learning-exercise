#!/usr/bin/env pytorch
# -*- coding: UTF-8 -*-
"""
@Project     :llearn_pytorch
@File        :model.py
@IDE         :PyCharm
@Author      :张世航
@Date        :2023/2/24 11:30
@Description :一个深度学习演示样例
"""
import torchvision.datasets
from torch.optim import lr_scheduler, SGD
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from model import *
import time

# 设置设备
zDevice = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# 加载数据集
zTrainData = torchvision.datasets.CIFAR10("TrainData", train=True, transform=torchvision.transforms.ToTensor(),
                                          download=True)
zTestData = torchvision.datasets.CIFAR10("TestData", train=False, transform=torchvision.transforms.ToTensor(),
                                         download=True)

# 添加记录者
writer = SummaryWriter("logs")

# 计算数据集长度
iTrainDataLength = len(zTrainData)
iTestDataLength = len(zTestData)
print("the length of train data :{}".format(iTrainDataLength))
print("the length of test data:{}".format(iTestDataLength))

# 利用dataloader加载数据集
zTraindataLoader = DataLoader(zTrainData, batch_size=64)
zTestDataLoader = DataLoader(zTestData, batch_size=64)

# 创建网络模型
myModel = MyModel()
myModel = myModel.to(zDevice)
# 创建损失函数
myLossFunction = nn.CrossEntropyLoss()
myLossFunction = myLossFunction.to(zDevice)
# 优化器
dLearnRate = 1e-2
myOptimizer = SGD(myModel.parameters(), lr=dLearnRate)
# 设置学习率衰减函数
MyScheduler = lr_scheduler.StepLR(myOptimizer, 50, gamma=0.5)

# 设置训练网络的一些参数
# 训练的总次数
iTotalTrainStep = 0
# 测试的总次数
iTotalTestStep = 0
# 训练的轮数
iEpoch = 300

# 记录开始时间
fStartTime = time.time()

for i in range(iEpoch):
    print("----第{}训练开始！！!----".format(i))
    myModel.train()
    for data in zTraindataLoader:
        images, targets = data
        images = images.to(zDevice)
        targets = targets.to(zDevice)
        outputs = myModel(images)
        loss = myLossFunction(outputs, targets)
        # 优化器优化模型
        myOptimizer.zero_grad()
        loss.backward()
        myOptimizer.step()
        iTotalTrainStep = iTotalTrainStep + 1
        if iTotalTrainStep % 100 == 0:
            fEndTime = time.time()
            print("第{}次模型训练loss是{}".format(iTotalTrainStep, loss.item()))
            writer.add_scalar("train_loss", loss.item(), iTotalTrainStep)
            print("训练耗时{}".format(fEndTime - fStartTime))
    MyScheduler.step()
    print("----调整学习率为{}----".format(myOptimizer.state_dict()['param_groups'][0]['lr']))
    writer.add_scalar("train_lr", myOptimizer.state_dict()['param_groups'][0]['lr'], i)

    myModel.eval()
    iTotalLoss = 0
    iTotalAccuracy = 0
    print("----第{}测试开始！！!----".format(i))
    with torch.no_grad():
        for data in zTestDataLoader:
            images, targets = data
            images = images.to(zDevice)
            targets = targets.to(zDevice)
            outputs = myModel(images)
            loss = myLossFunction(outputs, targets)
            iTotalLoss = iTotalLoss + loss.item()
            accuracy = (outputs.argmax(1) == targets).sum()
            iTotalAccuracy = accuracy + iTotalAccuracy

    print("第{}次模型测试loss是{}".format(i, iTotalLoss))
    print("第{}次模型测试正确率是{}".format(i, iTotalAccuracy / iTestDataLength))
    writer.add_scalar("test_loss", iTotalLoss, iTotalTestStep)
    writer.add_scalar("test_accuracy", iTotalAccuracy / iTestDataLength, iTotalTestStep)
    iTotalTestStep = iTotalTestStep + 1

    # 保存每一次训练的模型
    torch.save(myModel.state_dict(), "model/model_{}.path".format(i))
    print("----模型已经保存！！！----")

writer.close()
