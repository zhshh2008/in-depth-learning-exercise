#!/usr/bin/env pytorch
# -*- coding: UTF-8 -*-
"""
@Project     :llearn_pytorch 
@File        :testmodel.py
@IDE         :PyCharm 
@Author      :张世航
@Date        :2023/2/27 8:48 
@Description :一个简易的验证训练好的模型的程序
"""
import os

import torch
from PIL import Image
from torchvision import transforms
from model import MyModel


class myImage:
    def __init__(self, strRootDir):
        self.mstrRootDir = strRootDir
        self.mstrImagePath = os.listdir(self.mstrRootDir)

    def __getitem__(self, item):
        strImageName = self.mstrImagePath[item]
        strImageItemPath = os.path.join(self.mstrRootDir, strImageName)
        zImage = Image.open(strImageItemPath)
        strLabel = strImageName
        return zImage, strLabel

    def __len__(self):
        return len(self.mstrImagePath)


funTransform = transforms.Compose([
    transforms.Resize((32, 32)),
    transforms.ToTensor()
])

model = MyModel()
model.load_state_dict(torch.load("model/model_37.path"))
model.eval()
strTestDir = "testimage"
zData = myImage(strTestDir)
image_type = ("airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck", "nolen")
with torch.no_grad():
    for data in zData:
        image, label = data
        image = image.convert('RGB')
        image = funTransform(image)
        image = torch.reshape(image, (1, 3, 32, 32))
        output = model(image)
        iResult = output.argmax(1)
        print("图片类型|模型识别出类型:{}|{}".format(label, image_type[iResult]))
