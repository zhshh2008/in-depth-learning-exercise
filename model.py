#!/usr/bin/env pytorch
# -*- coding: UTF-8 -*-
'''
@Project     :llearn_pytorch 
@File        :model.py
@IDE         :PyCharm 
@Author      :张世航
@Date        :2023/2/24 11:30 
@Description :一个CIFAR10的简单模型
'''
import torch
from torch import nn


# 构建神经网络
class MyModel(nn.Module):

    def __init__(self):
        super(MyModel, self).__init__()
        self.model = nn.Sequential(
            # 卷积层1
            nn.Conv2d(3, 32, 5, 1, 2, ),
            # 池化层1
            nn.MaxPool2d(2),
            # 卷积层2
            nn.Conv2d(32, 32, 5, 1, 2),
            # 池化层2
            nn.MaxPool2d(2),
            # 卷积层3
            nn.Conv2d(32, 64, 5, 1, 2),
            # 池化层3
            nn.MaxPool2d(2),
            # 展平层
            nn.Flatten(),
            # 全连接层1
            nn.Linear(64 * 4 * 4, 64),
            # 全连接层2
            nn.Linear(64, 10)
        )

    def forward(self, x):
        """
        神经元向前传播函数
        :param x: 输入的参数
        :return: 输出的参数
        """
        x = self.model(x)
        return x


# 神经网络模型测试
if __name__ == '__main__':
    funModel = MyModel()
    myInPut = torch.ones(64, 3, 32, 32)
    myOutPut = funModel(myInPut)
    print(myOutPut.shape)
