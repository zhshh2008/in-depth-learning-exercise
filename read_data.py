from torch.utils.data import Dataset
from PIL import Image
import os

class myData(Dataset):
    def __init__(self, strRootDir, strLabelDir):
        self.mstrRootDir = strRootDir
        self.mstrLabelDir = strLabelDir
        self.mstrPath = os.path.join(strRootDir, strLabelDir)
        self.mstrImagePath = os.listdir(self.mstrPath)

    def __getitem__(self, item):
        strImageName = self.mstrImagePath[item]
        strImageItemPath = os.path.join(self.mstrPath, strImageName)
        zImage = Image.open(strImageItemPath)
        strLabel = self.mstrLabelDir
        return zImage, strLabel

    def __len__(self):
        return len(self.mstrImagePath)

strRootDir = "dateset/train"
strLabelAnts = "ants"
strLabelBees = "bees"
zAntsData = myData(strRootDir, strLabelAnts)
zBeesData = myData(strRootDir, strLabelBees)
zTrainData = zAntsData + zBeesData
print(len(zAntsData), len(zBeesData), len(zTrainData))




