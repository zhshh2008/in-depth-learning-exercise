from torch.utils.tensorboard import SummaryWriter
import numpy
from PIL import Image

wirter = SummaryWriter("logs")
PILImagePath = "dateset/train/ants/0013035.jpg"
PILImage = Image.open(PILImagePath)
ImageArray = numpy.array(PILImage)
wirter.add_image("ants", ImageArray, 1, dataformats='HWC')

for i in range(100):
    wirter.add_scalar("y = 2x", 2 * i, i)

wirter.close()

